﻿using CRUD_API.Data;
using CRUD_API.Data.Models;
using CRUD_API.Services.Contracts;
using CRUD_API.Utilities;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace CRUD_API.Services.Repositories
{
    public class AuthServices: ServiceBase, IAuthServices
    {
        private string JwtSecretKey;
        private SymmetricSecurityKey PrivateKey;

        public AuthServices(DataContext _dataContext, AppSettings _appSettings): base(_dataContext)
        {
            this.JwtSecretKey = _appSettings.Secret;
            this.PrivateKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.Secret));
        }

        public string GetSecretKey()
        {
            return this.JwtSecretKey;
        }

        public string GenerateToken(string username)
        {
            JwtSecurityToken jwtTokenObject = new JwtSecurityToken(
                claims: new Claim[] { new Claim(type: ClaimTypes.Name, username) },
                notBefore: new DateTimeOffset(DateTime.Now).DateTime,
                expires: new DateTimeOffset(DateTime.Now.AddMinutes(60)).DateTime,
                signingCredentials: new SigningCredentials(this.PrivateKey, SecurityAlgorithms.HmacSha256));

            string token = new JwtSecurityTokenHandler().WriteToken(jwtTokenObject);
            return token;
        }

        public string HashPassword(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password);
        }

        public async Task<Tuple<bool, string>> Register(Users newUser) 
        {
            try
            {
                newUser.Password = this.HashPassword(newUser.Password);
                await this.DataContext.Users.AddAsync(newUser);
                this.Commit();

                return Tuple.Create(true, GenerateToken(newUser.Username));
            } catch (Exception e)
            {
                Console.Error.WriteLine(e);
                return Tuple.Create(false, "Error trying to login!");
            }
        }

        public Tuple<bool, string> Login(string username, string password)
        {
            try
            {
                var userPassword = this.DataContext.Users.
                    Where(user => user.Username == username).
                    Select(user => user.Password).
                    FirstOrDefault();

                if (userPassword != null)
                {
                    bool valid = BCrypt.Net.BCrypt.Verify(password, userPassword);
                    if (valid) return Tuple.Create(valid, GenerateToken(username));
                }
                return Tuple.Create(false, "Invalid login attempt!");
            } catch (Exception e)
            {
                Console.Error.WriteLine(e);
                return Tuple.Create(false, "Error trying to register!");
            }
        }
    }
}
