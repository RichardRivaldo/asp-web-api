﻿using CRUD_API.Data;
using CRUD_API.Data.DTO;
using CRUD_API.Data.Models;
using CRUD_API.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System.Web.Http;

namespace CRUD_API.Services.Repositories
{
    public class ArticleServices : ServiceBase, IArticleServices
    {
        public ArticleServices(DataContext _dataContext) : base(_dataContext)
        {

        }

        public async Task<bool> CreateNewArticle(Articles newArticle)
        {
            try
            {
                await this.DataContext.Articles.AddAsync(newArticle);
                this.Commit();

                return true;
            }catch (Exception e)
            {
                Console.Error.WriteLine(e.Message);
                return false;
            }
        }

        public async Task<bool> DeleteArticle(string slug)
        {
            try
            {
                var article = await this.DataContext.Articles.
                    Where(article => article.Slug == slug).
                    FirstOrDefaultAsync();
                if (article != null)
                {
                    this.DataContext.Articles.Remove(article);
                    this.Commit();
                    return true;
                }
                return false;
            } catch (Exception e)
            {
                Console.Error.WriteLine(e);
                return false;
            }
        }

        public async Task<bool> EditArticle(Articles updatedArticle)
        {
            try
            {
                var entry = await this.GetArticleDetails(updatedArticle.Slug);
                entry.Title = updatedArticle.Title;
                entry.Content = updatedArticle.Content;
                this.Commit();
                return true;
            } catch (Exception e)
            {
                Console.Error.WriteLine(e);
                return false;
            }
        }

        public async Task<Articles> GetArticleDetails(string slug)
        {
            Articles article = await DataContext.Articles.Where(article => article.Slug == slug).FirstOrDefaultAsync();
            return article;
        }

        public async Task<IEnumerable<Articles>> GetUserArticles()
        {
            IEnumerable<Articles> articles = await this.DataContext.Articles.Select(article => article).ToListAsync();
            return articles;
        }

        public async Task<bool> IsArticleExist(string slug)
        {
            var article = await this.DataContext.Articles.Where(article => article.Slug == slug).FirstOrDefaultAsync();
            return article != null;
        }
    }
}
