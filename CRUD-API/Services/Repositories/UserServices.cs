﻿using CRUD_API.Data;
using CRUD_API.Data.Models;
using CRUD_API.Services.Contracts;
using Microsoft.EntityFrameworkCore;

namespace CRUD_API.Services.Repositories
{
    public class UserServices : ServiceBase, IUserServices
    {
        public IAuthServices authServices;
        public UserServices(DataContext _dataContext, IAuthServices _authServices) : base(_dataContext)
        {
            this.authServices = _authServices;
        }

        public bool CheckUserExist(Users user)
        {
            var exist = this.DataContext.Users.Any(u => u.Username == user.Username || u.Email == user.FullName);
            return exist;
        }

        public async Task<Tuple<bool, string>> CreateNewUser(Users newUser)
        {
            try
            {
                bool userExist = this.CheckUserExist(newUser);
                if (userExist)
                {
                    return Tuple.Create(false, "Invalid creation");
                }
                else
                {
                    (bool created, string token) = await this.authServices.Register(newUser);
                    if (created)
                    {
                        return Tuple.Create(true, token);
                    }
                    else { return Tuple.Create(false, "Error trying to register user"); }
                }
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.Message);
                return Tuple.Create(false, "Error trying to register user");
            }
        }

        public async Task<bool> DeleteUser(string username)
        {
            try
            {
                var user = await this.DataContext.Users.
                    Where(u => u.Username == username).
                    FirstOrDefaultAsync();
                if (user != null)
                {
                    this.DataContext.Users.Remove(user);
                    this.Commit();
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e);
                return false;
            }
        }

        public async Task<bool> UpdateUserProfile(Users updatedUser)
        {
            try
            {
                var entry = await this.GetUserDetails(updatedUser.Username);
                entry.Email = updatedUser.Email;
                entry.FullName = updatedUser.FullName;
                entry.Password = this.authServices.HashPassword(updatedUser.Password);
                entry.PhoneNumber = updatedUser.PhoneNumber;
                entry.BirthDate = updatedUser.BirthDate;
                this.Commit();
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e);
                return false;
            }
        }

        public async Task<IEnumerable<Users>> GetAllUsers()
        {
            IEnumerable<Users> users = await this.DataContext.Users.Select(user => user).ToListAsync();
            return users;
        }

        public async Task<Users> GetUserDetails(string username)
        {
            Users user = await this.DataContext.Users.Where(u => u.Username == username).FirstOrDefaultAsync();
            return user;
        }

        public Tuple<bool, string> Login(string username, string password)
        {
            try
            {
                (bool valid, string token) = this.authServices.Login(username, password);
                if (valid)
                {
                    return Tuple.Create(true, token);
                }
                else { return Tuple.Create(false, "Error trying to login user"); }
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.Message);
                return Tuple.Create(false, "Error trying to login user");
            }
        }
    }
}
