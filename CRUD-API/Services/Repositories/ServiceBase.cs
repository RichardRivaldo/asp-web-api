﻿using CRUD_API.Data;
using CRUD_API.Services.Contracts;

namespace CRUD_API.Services.Repositories
{
    public class ServiceBase : IServiceBase
    {
        public DataContext DataContext { get; set; }

        public ServiceBase(DataContext _dataContext)
        {
            this.DataContext = _dataContext;
        }

        public void Commit()
        {
            this.DataContext.SaveChanges();
        }
    }
}
