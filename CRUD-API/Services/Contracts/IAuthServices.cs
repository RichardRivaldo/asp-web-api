﻿using CRUD_API.Data.Models;

namespace CRUD_API.Services.Contracts
{
    public interface IAuthServices: IServiceBase
    {
        public string GetSecretKey();
        public string GenerateToken(string username);
        public string HashPassword(string password);
        public Task<Tuple<bool, string>> Register(Users newUser);
        public Tuple<bool, string> Login(string username, string password);
    }
}
