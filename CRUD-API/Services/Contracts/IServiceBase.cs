﻿using CRUD_API.Data;

namespace CRUD_API.Services.Contracts
{
    public interface IServiceBase
    {
        DataContext DataContext { get; set; }

        void Commit();
    }
}
