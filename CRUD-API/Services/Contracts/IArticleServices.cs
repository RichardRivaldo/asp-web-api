﻿using CRUD_API.Data.Models;
using CRUD_API.Services.Repositories;
using System.Web.Http;

namespace CRUD_API.Services.Contracts
{
    public interface IArticleServices: IServiceBase
    {
        Task<IEnumerable<Articles>> GetUserArticles();
        Task<Articles> GetArticleDetails(string slug);
        Task<bool> CreateNewArticle(Articles newArticle);
        Task<bool> EditArticle(Articles updatedArticle);
        Task<bool> DeleteArticle(string slug);
        Task<bool> IsArticleExist(string slug);
    }
}
