﻿using CRUD_API.Data.Models;

namespace CRUD_API.Services.Contracts
{
    public interface IUserServices: IServiceBase
    {
        Task<IEnumerable<Users>> GetAllUsers();
        Task<Users> GetUserDetails(string username);
        Task<Tuple<bool, string>> CreateNewUser(Users newUser);
        Tuple<bool, string> Login(string username, string password);
        Task<bool> UpdateUserProfile(Users updatedUser);
        Task<bool> DeleteUser(string username);
        bool CheckUserExist(Users user);
    }
}
