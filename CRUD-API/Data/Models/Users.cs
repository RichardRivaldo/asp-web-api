﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRUD_API.Data.Models
{
    public class Users
    {
        [Key]
        public Int16 UserID { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }

        [Column(TypeName = "Date")]
        public DateTime BirthDate { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
    }
}
