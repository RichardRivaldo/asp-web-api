﻿using System.ComponentModel.DataAnnotations;

namespace CRUD_API.Data.Models
{
    public class Articles
    {
        [Key]
        public Int16 PostID { get; set; }
        public string Title { get; set; }
        public string Slug { get; set; }
        public string Content { get; set; }
        public Int16 AuthorUserID { get; set; }
    }
}
