﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRUD_API.Data.DTO
{
    public class UsersDTO
    {
        [Key]
        /// <summary>
        /// User's username.
        /// </summary>
        public String UserName { get; set; }
        /// <summary>
        /// User's email.
        /// </summary>
        public String Email { get; set; }
        /// <summary>
        /// User's full name.
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// User's birth date. Sufficient example: YYYY-MM-DD.
        /// </summary>
        [Column(TypeName = "Date")]
        public DateTime BirthDate { get; set; }
        /// <summary>
        /// User's phone number.
        /// </summary>
        public string PhoneNumber { get; set; }
    }
}
