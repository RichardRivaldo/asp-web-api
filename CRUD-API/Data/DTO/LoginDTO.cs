﻿namespace CRUD_API.Data.DTO
{
    public class LoginDTO
    {
        /// <summary>
        /// User's username.
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// User's password.
        /// </summary>
        public string Password { get; set; }
    }
}
