﻿using System.ComponentModel.DataAnnotations;

namespace CRUD_API.Data.DTO
{
    public class ArticlesDTO
    {
        /// <summary>
        /// Title of the Content.
        /// </summary>
        [Key]
        public string Title { get; set; }

        /// <summary>
        /// Unique Slug.
        /// </summary>
        [RegularExpression("^[a-z]+(-[a-z]+)*$", ErrorMessage = "Please enter valid slug! Lowercase letters and hyphens only!")]
        public string Slug { get; set; }

        /// <summary>
        /// Content Body.
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Author ID.
        /// </summary>
        public Int16 AuthorUserID { get; set; }
    }
}
