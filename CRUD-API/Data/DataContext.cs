﻿using CRUD_API.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace CRUD_API.Data
{
    public class DataContext:DbContext
    {
        public DataContext(DbContextOptions<DataContext> options): base(options)
        {

        }

        public DbSet<Users> Users { get; set; }
        public DbSet<Articles> Articles { get; set; }
    }
}
