﻿using AutoMapper;
using CRUD_API.Data.DTO;
using CRUD_API.Data.Models;
using CRUD_API.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CRUD_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        public IUserServices userServices;
        public readonly IMapper _mapper;

        public UsersController(IUserServices _userServices, IMapper mapper) 
        {
            this.userServices = _userServices;
            this._mapper = mapper;
        }

        /// <summary>
        /// Get all users.
        /// </summary>
        /// <returns>All users.</returns>
        /// <response code="200">Returns all users.</response>
        /// <response code="204">No users found.</response>
        [Authorize]
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        public async Task<ResponseDTO<IEnumerable<UsersDTO>>> GetAllUsers()
        {
            var response = new ResponseDTO<IEnumerable<UsersDTO>>();
            var users = await this.userServices.GetAllUsers();
            var usersDTO = this._mapper.Map<IEnumerable<UsersDTO>>(users);
            if (usersDTO.Any())
            {
                response.StatusCode = StatusCodes.Status200OK;
                response.Message = "Data retrieved";
            }
            else
            {
                response.StatusCode = StatusCodes.Status204NoContent;
                response.Message = "No data retrieved";
            }
            response.Data = usersDTO;
            return response;
        }

        /// <summary>
        /// Get details of a certain user with certain username.
        /// </summary>
        /// <param name="username">User's username.</param>
        /// <returns>User details, password excluded.</returns>
        /// <response code="200">User details, password excluded.</response>
        /// <response code="404">Cannot find any user with the query username.</response>
        [Authorize]
        [HttpGet("{username}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<ResponseDTO<UsersDTO>> GetUserDetails(string username)
        {
            var response = new ResponseDTO<UsersDTO>();
            var userDetails = await this.userServices.GetUserDetails(username);
            var userDTO = this._mapper.Map<UsersDTO>(userDetails);
            if (userDTO != null)
            {
                response.StatusCode = StatusCodes.Status200OK;
                response.Message = "Data retrieved";
            }
            else
            {
                response.StatusCode = StatusCodes.Status404NotFound;
                response.Message = "No entry found";
            }
            response.Data = userDTO;
            return response;
        }

        /// <summary>
        /// Register a new user, specify all the parameters.
        /// </summary>
        /// <param name="Username">User's username. Must be unique.</param>
        /// <param name="Email">User's email. Must be unique.</param>
        /// <param name="FullName">User's full name.</param>
        /// <param name="BirthDate">User's birth date.</param>
        /// <param name="Password">User's password.</param>
        /// <param name="PhoneNumber">User's phone number.</param>
        /// <returns>Generated authentication token.</returns>
        /// <response code="201">Created the article successfully, return the authentication token.</response>
        /// <response code="400">Invalid request, either the username or email is already used or another errors occured.</response>
        [HttpPost("register")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<ResponseDTO<string>> CreateNewUser(ProfileDTO newUser)
        {
            var response = new ResponseDTO<string>();
            var user = this._mapper.Map<Users>(newUser);
            (bool created, string token) = await this.userServices.CreateNewUser(user);

            if (created)
            {
                response.StatusCode = StatusCodes.Status201Created;
                response.Message = "Successfully created";
                response.Data = token;
            }
            else
            {
                response.StatusCode = StatusCodes.Status400BadRequest;
                response.Message = "Failed creating new user";
            }
            return response;
        }

        /// <summary>
        /// Login to already registered account with username and password.
        /// </summary>
        /// <param name="username">User's username.</param>
        /// <param name="password">User's password.</param>
        /// <returns>Generated authentication token.</returns>
        /// <response code="200">Logged successfully, returns the authentication token.</response>
        /// <response code="400">Invalid login attempt, wrong password / username, or user doesn't exist.</response>
        [HttpPost("login")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public ResponseDTO<string> LoginUser(LoginDTO loginDTO)
        {
            var response = new ResponseDTO<string>();
            (bool valid, string token) = this.userServices.Login(loginDTO.Username, loginDTO.Password);
            if (valid)
            {
                response.StatusCode = StatusCodes.Status200OK;
                response.Message = "Successfully logged in";
                response.Data = token;
            } 
            else
            {
                response.StatusCode = StatusCodes.Status400BadRequest;
                response.Message = "Invalid login attempt";
            }
            return response;
        }

        /// <summary>
        /// Delete user with specified username.
        /// </summary>
        /// <param name="username">User's username.</param>
        /// <returns>No content.</returns>
        /// <response code="204">The user is successfully deleted.</response>
        /// <response code="500">Internal server errors when trying to delete the user or user not found.</response>
        [Authorize]
        [HttpDelete("delete/{username}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<ResponseDTO<UsersDTO>> DeleteUser(string username)
        {
            var response = new ResponseDTO<UsersDTO>();
            var deleted = await this.userServices.DeleteUser(username);
            if (deleted)
            {
                response.StatusCode = StatusCodes.Status204NoContent;
                response.Message = "User deleted successfully";
            }
            else
            {
                response.StatusCode = StatusCodes.Status500InternalServerError;
                response.Message = "Error trying to delete the user, or user not found";
            }
            return response;
        }

        /// <summary>
        /// Change user's profile. Username cannot be modified.
        /// </summary>
        /// <param name="Username">User's username. Cannot be modified.</param>
        /// <param name="Email">User's email. Must be unique.</param>
        /// <param name="FullName">User's full name.</param>
        /// <param name="BirthDate">User's birth date.</param>
        /// <param name="Password">User's password.</param>
        /// <param name="PhoneNumber">User's phone number.</param>
        /// <returns>No content.</returns>
        /// <response code="204">The user is successfully modified.</response>
        /// <response code="500">Internal server errors when trying to modify the user.</response>
        [Authorize]
        [HttpPatch("edit")]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<ResponseDTO<ProfileDTO>> PatchUser(ProfileDTO profileDTO)
        {
            var response = new ResponseDTO<ProfileDTO>();
            var user = this._mapper.Map<Users>(profileDTO);
            var edited = await this.userServices.UpdateUserProfile(user);
            if (edited)
            {
                response.StatusCode = StatusCodes.Status204NoContent;
                response.Message = "User profile updated";
            }
            else
            {
                response.StatusCode = StatusCodes.Status500InternalServerError;
                response.Message = "Error trying to update user profile";
            }
            return response;
        }
    }
}
