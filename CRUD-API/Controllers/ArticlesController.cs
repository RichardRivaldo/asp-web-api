﻿using AutoMapper;
using CRUD_API.Data.DTO;
using CRUD_API.Data.Models;
using CRUD_API.Services.Contracts;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace CRUD_API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ArticlesController : ControllerBase
    {
        public IArticleServices articlesService;
        public readonly IMapper _mapper;

        public ArticlesController(IArticleServices _articleServices, IMapper mapper)
        {
            this.articlesService = _articleServices;
            this._mapper = mapper;
        }

        /// <summary>
        /// Get all available articles. Might be changed to getting certain user's articles only.
        /// </summary>
        /// <returns>All users articles.</returns>
        /// <response code="200">Returns all articles.</response>
        /// <response code="204">No articles found.</response>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        public async Task<ResponseDTO<IEnumerable<ArticlesDTO>>> GetUserArticles()
        {
            var response = new ResponseDTO<IEnumerable<ArticlesDTO>>();
            var articles = await this.articlesService.GetUserArticles();
            var articlesDTO = this._mapper.Map<IEnumerable<ArticlesDTO>>(articles);
            if (articlesDTO != null && articlesDTO.Any())
            {
                response.StatusCode = StatusCodes.Status200OK;
                response.Message = "Data retrieved";
            }
            else
            {
                response.StatusCode = StatusCodes.Status204NoContent;
                response.Message = "No data retrieved";
            }
            response.Data = articlesDTO;
            return response;
        }

        /// <summary>
        /// Get details of a certain article with certain slug.
        /// </summary>
        /// <param name="slug">Article Slug.</param>
        /// <returns>Content details.</returns>
        /// <response code="200">Content details.</response>
        /// <response code="404">Cannot find any content with the query slug.</response>
        [HttpGet("{slug}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<ResponseDTO<ArticlesDTO>> GetArticleDetails(string slug)
        {
            var response = new ResponseDTO<ArticlesDTO>();
            var articleDetails = await this.articlesService.GetArticleDetails(slug);
            var articlesDTO = this._mapper.Map<ArticlesDTO>(articleDetails);
            if (articlesDTO != null)
            {
                response.StatusCode = StatusCodes.Status200OK;
                response.Message = "Data retrieved";
            } 
            else
            {
                response.StatusCode = StatusCodes.Status404NotFound;
                response.Message = "No entry found";
            }
            response.Data = articlesDTO;
            return response;
        }

        /// <summary>
        /// Create a new article with personalized title, content, and slug.
        /// </summary>
        /// <param name="title">Title of the article.</param>
        /// <param name="slug">Slug of the article. Hyphens and lowercase letters only.</param>
        /// <param name="content">Content body of the article.</param>
        /// <param name="authorUserID">User ID of the author.</param>
        /// <returns>Newly created article.</returns>
        /// <response code="201">Created the article successfully.</response>
        /// <response code="400">Invalid request, either the slug is already used or another error occured.</response>
        [HttpPost("create")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<ResponseDTO<ArticlesDTO>> CreateNewArticle(ArticlesDTO newArticle)
        {
            var response = new ResponseDTO<ArticlesDTO>();
            var article = this._mapper.Map<Articles>(newArticle);
            var created = await this.articlesService.CreateNewArticle(article);

            if (created)
            {
                response.StatusCode = StatusCodes.Status201Created;
                response.Message = "Successfully created";
                response.Data = newArticle;
            }
            else
            {
                response.StatusCode = StatusCodes.Status400BadRequest;
                response.Message = "Failed creating new article";
            }
            return response;
        }

        /// <summary>
        /// Delete article with specified slug.
        /// </summary>
        /// <param name="slug">The slug of the article being deleted.</param>
        /// <returns>No content.</returns>
        /// <response code="204">The article is successfully deleted.</response>
        /// <response code="404">Cannot find any article with the specified slug.</response>
        /// <response code="500">Internal server errors when trying to delete the article.</response>
        [HttpDelete("delete/{slug}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ResponseDTO<ArticlesDTO>> DeleteArticle(string slug)
        {
            var response = new ResponseDTO<ArticlesDTO>();
            var exist = await this.articlesService.IsArticleExist(slug);
            if (exist)
            {
                var deleted = await this.articlesService.DeleteArticle(slug);
                if (deleted)
                {
                    response.StatusCode = StatusCodes.Status204NoContent;
                    response.Message = "Article deleted successfully";
                }
                else
                {
                    response.StatusCode = StatusCodes.Status500InternalServerError;
                    response.Message = "Error trying to delete the article";
                }
            }
            else
            {
                response.StatusCode = StatusCodes.Status404NotFound;
                response.Message = "Article not found!";
            }
            return response;
        }

        /// <summary>
        /// Change the article title or content. Author ID and Slug cannot be modified.
        /// </summary>
        /// <param name="title">New title for the article.</param>
        /// <param name="slug">Slug of the article. Cannot be modified.</param>
        /// <param name="content">New content body of the article.</param>
        /// <param name="authorUserID">User ID of the author. Cannot be modified.</param>
        /// <returns>No content.</returns>
        /// <response code="204">The article is successfully modified.</response>
        /// <response code="404">Cannot find any article with the specified slug.</response>
        /// <response code="500">Internal server errors when trying to modify the article.</response>
        [HttpPatch("edit")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ResponseDTO<ArticlesDTO>> PatchArticle(ArticlesDTO articleDTO)
        {
            var response = new ResponseDTO<ArticlesDTO>();
            var exist = await this.articlesService.IsArticleExist(articleDTO.Slug);
            if (exist)
            {
                var article = this._mapper.Map<Articles>(articleDTO);
                var edited = await this.articlesService.EditArticle(article);
                if (edited)
                {
                    response.StatusCode = StatusCodes.Status204NoContent;
                    response.Message = "Article modified successfully";
                }
                else
                {
                    response.StatusCode = StatusCodes.Status500InternalServerError;
                    response.Message = "Error trying to modify the article";
                }
            }
            else
            {
                response.StatusCode = StatusCodes.Status404NotFound;
                response.Message = "Article not found!";
            }
            return response;
        }
    }
}
